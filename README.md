# vue-gitlab-api

A deadly simple Vue.js plugin to consume GitLab API.

Head to [the extensive JSDoc API documentation for vue-gitlab-api](https://clorichel.gitlab.io/vue-gitlab-api/Vue_GitLabAPI.html), or keep on reading for an easy process to integrate it in your application.

## Using it

### Install and import

```bash
npm install vue-gitlab-api
```

In your main application JS file (typically `main.js` if you are using [vue-cli](https://github.com/vuejs/vue-cli) webpack template), simply use the plugin:

```javascript
// vue-resource is needed too
import VueResource from 'vue-resource'
Vue.use(VueResource)

// import vue-gitlab-api
import GitLabAPI from 'vue-gitlab-api'
Vue.use(GitLabAPI, { url: 'https://gitlab.com', token: 'user Private Token or Personal Access Token' })
```

You can configure _application wide_ options while `Vue.use`'ing this plugin:

| Name    | Description                                                                           |
|---------|---------------------------------------------------------------------------------------|
| `url`   | your GitLab instance URL (defaults to https://gitlab.com)                             |
| `token` | your GitLab user _Private Token_ or _Personal Access Token_ use to connect to the API |

### Consume GitLab API

From anywhere now, you can simply consume GitLab API:

```javascript
Vue.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
```

You can also use it in your `.vue` components with `this.GitLabAPI`:

```javascript
this.GitLabAPI.get('/projects', {}, [this.myGitLabData, 'projects'])
```

That was it! Now that you are bootstrapped in, have a look at all the methods available to you in the extensive JSDoc API documentation available on [these auto-generated GitLab Pages](https://clorichel.gitlab.io/vue-gitlab-api/Vue_GitLabAPI.html).

**Important:** if you want your filled-in property `this.myGitLabData.projects` to be reactive "the Vue.js way", you MUST define this variable as a data in your components or vues, with a default value of an empty object (read `myGitLabData: {}`). See how to do it on a vue component on this example:

```javascript
<template>
  <div>
    <p>Projects grabbed: {{ projectsCount }}</p>
  </div>
</template>

<script>
export default {
  data () {
    return {
      myGitLabData: {}
    }
  },
  mounted: function () {
    this.GitLabAPI.get('projects', {}, [this.myGitLabData, 'projects'])
  },
  computed: {
    projectsCount: function () {
      if (this.myGitLabData.projects) {
        return this.myGitLabData.projects.length
      }
      return 'none yet...'
    }
  }
}
</script>
```

This is due to the fact Vue does not allow to add new reactive properties dynamically. Read more about it on the awesome [Vue.js guide](http://vuejs.org/guide/reactivity.html#Change-Detection-Caveats).

During your application workflow, you could want to change GitLab instance URL, and/or user authentication token. These methods will let you do it easily:

```javascript
// set application wide GitLabAPI url value
this.GitLabAPI.setUrl('https://your.gitlab-instance.com')

// set application wide GitLabAPI token value
this.GitLabAPI.setToken('other user Private Token or Personal Access Token')
```

## Vuex store module

Using Vuex? You can attach _vue-gitlab-api_ Vuex module to your application store. Within one of your `.vue` components, simply register it before you using it from your application:

```javascript
// registers GitLabAPI Vuex module to your application Vuex store
this.GitLabAPI.registerStore(this.$store)

// you are now able to read this state
this.$store.state.GitLabAPI.downloading
```

Here are Vuex `states` provided by _vue-gitlab-api_ to your application:

| Vuex state    | Type    | Description                                        |
|---------------|---------|----------------------------------------------------|
| `downloading` | Boolean | Defines if vue-gitlab-api is currently downloading |
| `running`     | Number  | Count vue-gitlab-api requests running now          |

Have a look at [Vuex](https://github.com/vuejs/vuex) for more details, or read on this complete example:

```javascript
import Vue from 'vue'

// your application is using Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

// remember vue-resource is needed for vue-gitlab-api
import VueResource from 'vue-resource'
Vue.use(VueResource)

// using vue-gitlab-api
import GitLabAPI from 'vue-gitlab-api'
Vue.use(GitLabAPI, { url: 'https://gitlab.com', token: 'user Private Token' })

// declare your Vuex store
const store = new Vuex.Store({})

// this is your application
const app = new Vue({
  el: '#app',
  mounted: {
    // register GitLabAPI Vuex store!
    this.GitLabAPI.registerStore(store)
  },
  computed: {
    // with this computed property, do something insanely simple such as:
    // <p v-if="downloading">Downloading from GitLab...</p>
    downloading: function () {
      if (typeof store.state.GitLabAPI !== 'undefined') {
        return store.state.GitLabAPI.downloading
      } else {
        return false
      }
    }
  }
})
```

## Contributing

Initial scaffolding was done with [vue-cli](https://github.com/vuejs/vue-cli) webpack template. For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Simply said, one can start working on its own customized version of _vue-gitlab-api_ in no time:

``` bash
# install dependencies
npm install

# run unit tests
npm run unit

# build JSDoc API documentation (defaults to the ./out folder)
./node_modules/.bin/jsdoc src/GitLabAPI.js

# serve with hot reload at localhost:8080
npm run dev
```

## What's next?

Without any obligation nor due date, one could expect to be done this non-exhaustive [list of improvements grouped on issues labeled `Feature`](https://gitlab.com/clorichel/vue-gitlab-api/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=Feature).

You can read on the [Changelog](CHANGELOG.md) too, for historical and upcoming new features, changes, deprecations, removed features, bug and security fixes.

## Support

Your are free to [open an issue](https://gitlab.com/clorichel/vue-gitlab-api/issues/new) right in this GitLab repository whether you should be facing a problem or a bug. Please advise this is not a commercial product, so one could experience random response time. Positive, friendly and productive conversations are expected on the issues. Screenshots and steps to reproduce are highly appreciated. Chances are you may get your issue solved if you follow these simple guidelines.


## Credits

- Logo derived from a [SlideGenius](https://thenounproject.com/slidegenius/) creation [CC licensed](https://creativecommons.org/licenses/by/3.0/us/)

## License

The _vue-gitlab-api_ plugin is distributed under the [MIT License (MIT)](LICENSE). Please have a look at the dependencies licenses if you plan on using, building, or distributing this plugin.